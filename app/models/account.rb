# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  keyId      :string(255)      not null
#  vCode      :string(255)      not null
#  user_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

class Account < ActiveRecord::Base
  belongs_to :user
  has_many :characters, dependent: :destroy

  validates :name, presence: true
  validates :keyId, presence: true
  validates :vCode, presence: true
end
