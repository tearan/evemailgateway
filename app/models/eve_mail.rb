# == Schema Information
#
# Table name: eve_mails
#
#  messageID          :integer          not null, primary key
#  characterID        :string(255)      not null
#  senderID           :integer
#  sentDate           :datetime
#  title              :string(255)
#  toCorpOrAllianceID :string(255)
#  toCharacterIDs     :string(255)
#  toListID           :string(255)
#  body               :text
#  forwarded_at       :datetime
#  created_at         :datetime
#  updated_at         :datetime
#

require 'reve'

class EveMail < ActiveRecord::Base
  self.primary_key = 'messageID'
  belongs_to :character, foreign_key: "characterID"
  validates :characterID, presence: true

  scope :for_character, ->(id) { where(:characterID => id) }
  scope :incomplete, -> { where("body is null or body = ''") }
  scope :unsent, -> { where("forwarded_at is null") }

  def self.create_from_reve_mailmessage(msg, character_id)
    if evemail = self.find_by(messageID: msg.id) 
      # may want to update record if the attributes differ from the new XML data
      return evemail
    else
      attrs = {characterID: character_id,
        messageID: msg.id, 
        senderID: msg.sender_id, 
        sentDate: msg.send_date,
        title: msg.title, 
        toCorpOrAllianceID: msg.to_corp_or_alliance_id, 
        toCharacterIDs: msg.to_character_ids.to_s, 
        toListID: msg.to_list_ids.to_s }
      return self.create(attrs)
    end    
  end
  
  def self.retrieve_mail_headers(api_conn, characterID)
    api_conn.personal_mail_messages(:characterid => characterID)
  end

  def self.retrieve_mail_bodies(api_conn, characterID, message_ids)
    api_conn.personal_mail_message_bodies(:characterid => characterID, :ids => message_ids)
  end

  def self.create_eve_mails_from_headers(api_conn, characterID)
    mail_headers = self.retrieve_mail_headers(api_conn, characterID)
    if mail_headers.nil? 
      return nil
    end
    # create EveMail objects
    mail_headers.collect{|header| self.create_from_reve_mailmessage(header, characterID)} 
  end

  def self.fill_in_mail_bodies(api_conn, characterID, message_ids)
    bodies = self.retrieve_mail_bodies(api_conn, characterID, message_ids)
    if bodies
      bodies.each do |messageID, body| 
        if eve_mail = self.find_by(messageID: messageID)
          eve_mail.body = body
          eve_mail.save
        end
      end
    end
  end

  def self.check_for_new_eve_mail(api_conn, characterID)
    self.create_eve_mails_from_headers(api_conn, characterID)
    # Get bodies if we need to
    incompletes = self.incomplete.for_character(characterID)
    if incompletes
      message_ids = incompletes.collect(&:messageID)
      self.fill_in_mail_bodies(api_conn, characterID, message_ids)
    end
    # return any EveMail records we haven't forwarded yet
    self.unsent.for_character(characterID)
  end

  # Construct an email message to foward this to user
  def forward_to_email
    
  end
end
