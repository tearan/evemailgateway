# == Schema Information
#
# Table name: characters
#
#  characterID :string(255)      not null, primary key
#  name        :string(255)      not null
#  account_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class Character < ActiveRecord::Base
  self.primary_key = 'characterID'
  belongs_to :account
  has_one :user, through: :account
  has_many :eve_mails, foreign_key: "characterID", dependent: :destroy
  
  validates :name, presence: true
  validates :characterID, presence: true
  # CNK I would like to require account_id except I plan to use this
  # table to store lookup information for characters belonging to
  # accounts we don't manage, e.g. characters mentioned in emails

  def api_connection=(api)
    @api = api
  end
  def api_connection
    @api ||= Reve::API.new(account.keyId, account.vCode, characterID) 
  end

end
