class CharactersController < ApplicationController
  def show
    begin
      @character = Character.includes(:eve_mails).find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to "/404.html"
    end
  end
end
