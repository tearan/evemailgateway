class UsersController < Clearance::UsersController
  def show
    begin
      @user = User.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to "/404.html"
    end
  end

  # CNK needed to have the method in this file or I get the parent's user_from_params method
  def create
    super
  end

  # def edit
  #   @user = user_from_params
  #   render :template => 'users/new'
  # end

  # def update
  #   @user = user_from_params

  #   if @user.save
  #     redirect_back_or url_after_create
  #   else
  #     render :template => 'users/new'
  #   end
  # end

  private
    def user_from_params
      user_params = params[:user] || Hash.new
      first_name = user_params.delete(:first_name)
      last_name = user_params.delete(:last_name)
      email = user_params.delete(:email)
      password = user_params.delete(:password)
      
      Clearance.configuration.user_model.new(user_params).tap do |user|
        user.first_name = first_name
        user.last_name = last_name
        user.email = email
        user.password = password
      end
    end

end
