class AccountsController < ApplicationController
  def new
    @account = Account.new
  end

  def create
    @account = Account.new(account_params)
    @account.user_id = current_user.id
    if @account.save
      flash[:notice] = "Successfully created the account #{@account.name}"
      redirect_to user_path(current_user)
    else
      render :new
    end
  end

  def edit
    find_and_validate_account
  end

  def update
    find_and_validate_account
    @account.update(account_params)
    if @account.save
      flash[:notice] = "Successfully updated #{@account.name}"
      redirect_to user_path(current_user)
    else
      render :edit
    end
  end

  def destroy
    find_and_validate_account
    @account.destroy!
    flash[:notice] = "Removed account #{@account.name} and all the characters and emails associated with it."
    redirect_to user_path(current_user)
  end

  private

  def account_params
    params.require(:account).permit(:name, :keyId, :vCode)
  end

  def find_and_validate_account
    begin
      @account = Account.find(params[:id])
    rescue ActiveRecord::RecordNotFound
      redirect_to('/404.html')
      return
    end
    # Claim not to know about this account if it doesn't belong to the logged in user
    if @account.user_id != current_user.id
      redirect_to('/404.html')
    end
  end

end
