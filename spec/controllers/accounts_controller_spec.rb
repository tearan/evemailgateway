require 'spec_helper'

describe AccountsController do
  before(:each) do
    @user = FactoryGirl.create(:user)
    sign_in_as(@user)
  end

  describe "GET new" do
    it "creates a new (empty) account object" do
      get :new
      expect(assigns(:account)).to be_a_new(Account)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new account object" do
        expect {
          post :create, {:account => FactoryGirl.attributes_for(:account)}
        }.to change(Account, :count).by(1)
      end
      it "assigns the new account to the logged in user" do
        post :create, {:account => FactoryGirl.attributes_for(:account)}
        expect(assigns(:account).user_id).to eq @user.id
      end
      it "redirects back to user page" do
        post :create, {:account => FactoryGirl.attributes_for(:account)}
        expect(response).to redirect_to(user_path(@user.id))
      end
    end

    describe "with invalid params" do
      it "does not create a new account object" do
        expect {
          post :create, {:account => {:keyId => ''} }
        }.to change(Account, :count).by(0)
      end
      it "redirects back to account entry form" do
        post :create, {:account => {:keyId => ''} }
        expect(response).to render_template("new")
      end
    end
  end

  describe "GET edit" do
    before(:each) do
      @account = FactoryGirl.create(:account, :user => @user)
    end

    context "with valid account id" do
      it "retrieves the account object" do
        get :edit, :id => @account.id
        expect(assigns(:account)).to eq @account
      end
      it "renders the account edit form" do
        get :edit, :id => @account.id
        expect(response).to render_template("edit")
      end
    end

    context "with invalid account id" do
      it "returns a 404 if the account id is invalid" do
        get :edit, :id => @account.id + 1
        expect(response).to redirect_to("/404.html")
      end
      it "returns a 404 if the account does not belong to the current user" do
        random_account = FactoryGirl.create(:account)
        get :edit, :id => random_account.id
        expect(response).to redirect_to("/404.html")
      end
    end
  end

  describe "PUT update" do
    before(:each) do
      @account = FactoryGirl.create(:account, :user => @user)
    end

    context "with valid updates" do
      it "redirects to the user page" do
        put :update, {:id => @account.id, :account => {:name => 'New name'}}
        expect(response).to redirect_to(user_path(@user.id))
      end

      it "can change the account name" do
        put :update, {:id => @account.id, :account => {:name => 'New name'}}
        reloaded = Account.find(@account.id)
        expect(reloaded.name).to eq "New name"
      end
    end

    context "with invalid updates" do
      it "redirects to the account edit page" do
        put :update, {:id => @account.id, :account => {:name => ''}}
        expect(response).to render_template("edit")
      end

      it "shows validation error message" do
        put :update, {:id => @account.id, :account => {:name => ''}}
        expect(assigns(:account).errors).to include :name
      end
    end
  end

  describe "POST destroy" do
    before(:each) do
      @account = FactoryGirl.create(:account, :user => @user)
    end

    it "decreases the number of accounts by 1" do
      expect {
        delete :destroy, {:id => @account.id}
      }.to change(Account, :count).by(-1)
    end
  end
end
