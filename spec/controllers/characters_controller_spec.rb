require 'spec_helper'

describe CharactersController do
  describe "GET show" do
    context "with valid id" do
      before(:each) do
        @character = FactoryGirl.create(:character)
      end
      it "responds successfully" do
        get :show, id: @character.id
        expect(response).to be_success
      end
      it "loads the character object" do
        get :show, id: @character.id
        expect(assigns(:character)).to eq @character
      end
    end

    context "with an invalid id" do
      it "responds not found" do
        get :show, id: 666
        expect(response).to redirect_to("/404.html")
      end
    end
  end
end
