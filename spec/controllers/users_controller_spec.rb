require 'spec_helper'

describe UsersController do
  describe "GET show" do
    context "with valid id" do
      before(:each) do
        @u = FactoryGirl.create(:user)
      end
      it "responds successfully" do
        get :show, id: @u.id
        expect(response).to be_success
      end
      it "loads the user object" do
        get :show, id: @u.id
        expect(assigns(:user)).to eq @u
      end
    end

    context "with an invalid id" do
      it "responds not found" do
        get :show, id: 666
        expect(response).to redirect_to("/404.html")
      end
    end
  end
end
