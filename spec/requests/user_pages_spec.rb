require 'spec_helper'

describe "user's home page" do
  it "identifies the user" do
    me = FactoryGirl.create(:user)
    get "/users/#{me.id}"
    assert_select 'h1', :text => "#{me.first_name} #{me.last_name}"
    assert_select "a[href]", me.email
  end

  it "shows a list of characters" do
    me = FactoryGirl.create(:user_with_a_character)
    char = me.characters[0]
    get "/users/#{me.id}"
    assert_select "li" do
      assert_select "a", char.name
    end
  end
end
