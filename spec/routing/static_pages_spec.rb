require 'spec_helper'

describe "routing to home page" do
  it "routes / to static_pages#home" do
    expect(:get => "/").to route_to(controller: "static_pages", action: "home")
  end
end
