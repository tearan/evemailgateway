# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  keyId      :string(255)      not null
#  vCode      :string(255)      not null
#  user_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Account do
  it { should belong_to(:user) }
  it { should have_many(:characters) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:keyId) }
  it { should validate_presence_of(:vCode) }

  it "should delete its characters when it is deleted" do
    user = FactoryGirl.create(:user)
    account = FactoryGirl.create(:account, user: user)
    character = FactoryGirl.create(:character, account: account)
    # the next 5 are redundant but check I used FactoryGirl correctly to set up my situation
    user.should be_valid
    account.should be_valid
    account.user.should eq(user)
    character.should be_valid
    character.account.should eq(account)
    expect{account.destroy}.to change{Character.count}.by(-1)
  end

  it "should delete its characters when the user it belongs to is deleted" do
    user = FactoryGirl.create(:user)
    account = FactoryGirl.create(:account, user: user)
    character= FactoryGirl.create(:character, account: account)
    expect{user.destroy}.to change{Character.count}.by(-1)
  end
end
