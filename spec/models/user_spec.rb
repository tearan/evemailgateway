# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  first_name         :string(255)      not null
#  last_name          :string(255)      not null
#  email              :string(255)      not null
#  created_at         :datetime
#  updated_at         :datetime
#  encrypted_password :string(128)
#  confirmation_token :string(128)
#  remember_token     :string(128)
#

require 'spec_helper'

describe User do
  it { should have_many(:accounts) }
  it { should have_many(:characters).through(:accounts) }
  
  it { should validate_presence_of(:first_name) }
  it { should validate_presence_of(:last_name) }
  it { should validate_presence_of(:email) }

  it "should validate uniqueness of email" do
    attrs = FactoryGirl.attributes_for(:user)
    user1 = User.create(attrs) 
    user1.should be_valid
    user2 = User.new(attrs) 
    user2.should_not be_valid
    user2.errors_on(:email).should include("has already been taken")
  end
  
  it "should delete its accounts when it is deleted" do
    user = FactoryGirl.create(:user)
    account = FactoryGirl.create(:account, user: user)
    character= FactoryGirl.create(:character, account: account)
    # the next 5 are redundant but check I used FactoryGirl correctly to set up my situation
    user.should be_valid
    account.should be_valid
    account.user.should eq(user)
    character.should be_valid
    character.account.should eq(account)
    expect{user.destroy}.to change{Account.count}.by(-1)
  end
end
