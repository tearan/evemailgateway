# == Schema Information
#
# Table name: characters
#
#  characterID :string(255)      not null, primary key
#  name        :string(255)      not null
#  account_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#

require 'spec_helper'

describe Character do
  it { should belong_to(:account) }
  it { should have_one(:user).through(:account) }
  it { should have_many(:eve_mails) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:characterID) }

  it "should delete its eve_mails when it is deleted" do
    character = FactoryGirl.create(:character)
    evemail = FactoryGirl.create(:eve_mail, character: character)
    expect{character.destroy}.to change{EveMail.count}.by(-1)
  end

  it "should have one Reve API connection" do
    character = FactoryGirl.create(:character)
    character.should respond_to(:api_connection)
    character.api_connection.class.should eq Reve::API
  end

  it "can have it's Reve API connection overridden" do
    character = FactoryGirl.create(:character)
    character.api_connection = OpenStruct.new
    character.api_connection.class.should eq OpenStruct
  end
end
