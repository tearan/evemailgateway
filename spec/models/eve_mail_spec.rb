# == Schema Information
#
# Table name: eve_mails
#
#  messageID          :integer          not null, primary key
#  characterID        :string(255)      not null
#  senderID           :integer
#  sentDate           :datetime
#  title              :string(255)
#  toCorpOrAllianceID :string(255)
#  toCharacterIDs     :string(255)
#  toListID           :string(255)
#  body               :text
#  forwarded_at       :datetime
#  created_at         :datetime
#  updated_at         :datetime
#

require 'spec_helper'
require 'reve'

describe EveMail do
  it { should belong_to(:character) }
  it { should validate_presence_of(:characterID) }

  before(:each) do
    @character = FactoryGirl.create(:character)
  end

  describe ".incomplete scope" do
    it "should identify messages without message bodies" do
      incomplete_evemail = FactoryGirl.create(:eve_mail)
      complete_evemail = FactoryGirl.create(:eve_mail, body: 'Message body.' )

      results = EveMail.incomplete.to_a
      results.should include(incomplete_evemail)
      results.should_not include(complete_evemail)
    end
  end

  describe ".unsent scope" do
    it "should identify messages without message bodies" do
      forwarded_evemail = FactoryGirl.create(:eve_mail, body: 'Message body.', forwarded_at: Time.now)
      unforwarded_evemail = FactoryGirl.create(:eve_mail, body: 'Message body.')

      results = EveMail.unsent.to_a
      results.should include(unforwarded_evemail)
      results.should_not include(forwarded_evemail)
    end
  end
  
  describe ".for_character scope" do
    it "should identify messages sent to a specific character" do
      character1 = FactoryGirl.create(:character)
      evemail1 = FactoryGirl.create(:eve_mail, character: character1)
      evemail2 = FactoryGirl.create(:eve_mail)

      results = EveMail.for_character(character1).to_a
      results.should include(evemail1)
      results.should_not include(evemail2)
    end
  end
  
  describe ".create_from_reve_mailmessage" do
    context 'when called with a message header we haven\'t seen before' do
      it 'returns a new eve_mail instance' do 
        msg = Factory.build_reve_mailmessage
        evemail = EveMail.create_from_reve_mailmessage(msg, 3)
        evemail.title.should eq msg.title
        evemail.created_at.should be_within(1).of(DateTime.now)
      end
    end
    context 'when called with a message header we have seen before' do
      it 'returns the existing eve_mail instance' do
        msg = Factory.build_reve_mailmessage
        evemail = EveMail.create_from_reve_mailmessage(msg, 3)
        same_evemail = EveMail.create_from_reve_mailmessage(msg, 3)
        evemail.should eql(same_evemail)
      end
    end
  end

  describe ".fill_in_mail_bodies" do
    it "should query the Eve API for the message bodies" do
      @incomplete_evemail = FactoryGirl.create(:eve_mail, character: @character)
      api_connection = double()
      api_connection.should_receive(:personal_mail_message_bodies).with(:characterid => @character.characterID, :ids => [@incomplete_evemail.id])
      EveMail.fill_in_mail_bodies(api_connection, @character.characterID, [@incomplete_evemail.id])
    end

    it "should update messages with the message bodies from Eve" do
        Reve::API.personal_mail_messages_url = Rails.root.to_s + '/spec/fixtures/eve_requests/emails/headers_for_first_request.xml'
      Reve::API.personal_mail_message_bodies_url = Rails.root.to_s + '/spec/fixtures/eve_requests/emails/bodies_for_first_request.xml'
      api_connection = @character.api_connection
      EveMail.create_eve_mails_from_headers(api_connection, @character.characterID)  # set up messages from xml headers
      @incomplete_evemail = EveMail.find(290285275)
      EveMail.fill_in_mail_bodies(api_connection, @character.characterID, [@incomplete_evemail.id])
      @incomplete_evemail.reload.body.should eq "<p>Another message - more personal.</p>"
    end
  end

  describe ".retrieve_mail_headers" do
    before(:each) do
        Reve::API.personal_mail_messages_url = Rails.root.to_s + '/spec/fixtures/eve_requests/emails/headers_for_first_request.xml'
    end

    it "should retrieve mail headers from Eve" do
      api_connection = double()
      api_connection.should_receive(:personal_mail_messages).with(:characterid => @character.characterID)
      EveMail.retrieve_mail_headers(api_connection, @character.characterID)
    end

    it "should return new Reve MailMessage objects" do
      headers = EveMail.retrieve_mail_headers(@character.api_connection, @character.characterID)
      headers.size.should eq 2
      headers[0].class.should eq Reve::Classes::MailMessage
    end
  end

  describe ".check_for_new_eve_mail" do
    context "if I have never asked for mail" do
      before(:each) do
        Reve::API.personal_mail_messages_url = Rails.root.to_s + '/spec/fixtures/eve_requests/emails/headers_for_first_request.xml'
      Reve::API.personal_mail_message_bodies_url = Rails.root.to_s + '/spec/fixtures/eve_requests/emails/bodies_for_first_request.xml'
        @api_connection = @character.api_connection
      end

      it "should create EveMail records with bodies for each row from the EveAPI" do
        EveMail.check_for_new_eve_mail(@api_connection, @character.characterID)
        eve_mails = EveMail.all.to_a
        eve_mails.size.should eq 2
        eve_mails[0].body.should eq "Hi.<br><br>This is a message about something corporate.<br><br>"
      end

      it "should return both records" do
        result = EveMail.check_for_new_eve_mail(@api_connection, @character.characterID)
        result.size.should eq 2
      end
    end

    context "if I ask again for new EveMail" do
      before(:each) do
        # create preconditions of the objects from the first request
        create_records_from_first_request

        # And set up for second request
        Reve::API.personal_mail_messages_url = Rails.root.to_s + '/spec/fixtures/eve_requests/emails/headers_for_second_request.xml'
      Reve::API.personal_mail_message_bodies_url = Rails.root.to_s + '/spec/fixtures/eve_requests/emails/bodies_for_second_request.xml'
        @api_connection = @character.api_connection
      end

      it "should create EveMail records with bodies for each new row from the EveAPI" do
        EveMail.count.should eq 2
        EveMail.check_for_new_eve_mail(@api_connection, @character.characterID)
        EveMail.count.should eq 5
      end

      it "should return only the 3 new eve mails we got in the second request" do
        result = EveMail.check_for_new_eve_mail(@api_connection, @character.characterID)
        result.size.should eq 3
      end

      it "should not create EveMail records for rows we had already retrieved" do
        eve_mails_from_first_query = EveMail.all.to_a.map{|e| e.id}
        Rails.logger.debug "####### before we requery"
        Rails.logger.debug eve_mails_from_first_query
        results = EveMail.check_for_new_eve_mail(@api_connection, @character.characterID)
        eve_mails_from_second_query = results.map{|e| e.id}
        all_ids = EveMail.all.to_a.map{|e| e.id}
        (eve_mails_from_first_query + eve_mails_from_second_query).sort.should eq all_ids.sort
      end
    end

    # context "if we are still within the API caching period" do
    #   it "should ??" 
    # end
  end

  def create_records_from_first_request
    Reve::API.personal_mail_messages_url = Rails.root.to_s + '/spec/fixtures/eve_requests/emails/headers_for_first_request.xml'
    Reve::API.personal_mail_message_bodies_url = Rails.root.to_s + '/spec/fixtures/eve_requests/emails/bodies_for_first_request.xml'
    api_connection = @character.api_connection
    EveMail.check_for_new_eve_mail(api_connection, @character.characterID)  
    # make sure that we created 2 EveMail records by this helper
    existing_eve_mails = EveMail.all.to_a
    existing_eve_mails.count.should eq 2
    # and pretend we sent them
    existing_eve_mails.each{ |mail| mail.update_attributes({forwarded_at: DateTime.new}) }
  end

end
