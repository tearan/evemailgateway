# == Schema Information
#
# Table name: users
#
#  id                 :integer          not null, primary key
#  first_name         :string(255)      not null
#  last_name          :string(255)      not null
#  email              :string(255)      not null
#  created_at         :datetime
#  updated_at         :datetime
#  encrypted_password :string(128)
#  confirmation_token :string(128)
#  remember_token     :string(128)
#

FactoryGirl.define do

  sequence(:email) { |n| "user#{n}@example.com" }

  factory :user do
    first_name "John"
    last_name  "Doe"
    email      { generate(:email) } 
    password   "password"

    factory :user_with_a_character do
      after(:create) do |user, evaluator| 
        FactoryGirl.create_list(:account_with_a_character, 1, user: user)
      end
    end
  end

end
