# == Schema Information
#
# Table name: accounts
#
#  id         :integer          not null, primary key
#  name       :string(255)      not null
#  keyId      :string(255)      not null
#  vCode      :string(255)      not null
#  user_id    :integer          not null
#  created_at :datetime
#  updated_at :datetime
#

# Defined in this file: factories for user, account, and characters
FactoryGirl.define do

  sequence(:keyid) {|n| (0..9).to_a.shuffle[0,6].join }
  sequence(:vcode) {|n| ('A'..'z').to_a.shuffle[0,30].join }

  factory :account do
    name   "John's Account"
    keyId  { generate(:keyid) }
    vCode  { generate(:vcode) }
    user

    factory :account_with_a_character do
      after(:create) do |account, evaluator| 
        FactoryGirl.create_list(:character, 1, account: account)
      end
    end
  end
end
