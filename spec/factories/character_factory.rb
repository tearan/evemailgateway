# == Schema Information
#
# Table name: characters
#
#  characterID :string(255)      not null, primary key
#  name        :string(255)      not null
#  account_id  :integer
#  created_at  :datetime
#  updated_at  :datetime
#

FactoryGirl.define do

  sequence(:characterid) {|n| (0..9).to_a.shuffle[0,8].join }
  
  factory :character do
    characterID  { generate(:characterid)} 
    name         { "Character #{characterID}" }
    account
  end

end
