require 'reve'

class Factory

  def self.build_reve_mailmessage( overrides={} )
    attrs =  { 'characterID' => 123456,
      'messageID' => 123456789,
      'senderID' =>  909877543,
      'sentDate' =>  "2013-06-09 190800",
      'title' =>     "Message title",
      'toCorpOrAllianceID' => "",
      'toCharacterIDs' =>     "",
      'toListID' =>           ""}.merge(overrides)

    Reve::Classes::MailMessage.new(attrs)
  end

end
