# == Schema Information
#
# Table name: eve_mails
#
#  messageID          :integer          not null, primary key
#  characterID        :string(255)      not null
#  senderID           :integer
#  sentDate           :datetime
#  title              :string(255)
#  toCorpOrAllianceID :string(255)
#  toCharacterIDs     :string(255)
#  toListID           :string(255)
#  body               :text
#  forwarded_at       :datetime
#  created_at         :datetime
#  updated_at         :datetime
#

# Defined in this file: factories for user, account, and characters
FactoryGirl.define do
  sequence(:messageID) {|n| (0..9).to_a.shuffle[0,9].join }
  sequence(:senderID) {|n| (0..9).to_a.shuffle[0,10].join }

  factory :eve_mail do
    character
    messageID     { generate(:messageID) }
    senderID      { generate(:senderID) }
    sentDate      { Date.new }
    title         "Message title"
    toCorpOrAllianceID ""
    toCharacterIDs     ""
    toListID           ""
  end

end
