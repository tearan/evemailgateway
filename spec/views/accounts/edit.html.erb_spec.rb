require 'spec_helper'

describe "accounts/edit" do
  before(:each) do
    assign(:account, stub_model(Account, :id => 99))
  end

  it "should display an account update form" do
    render
    expect(rendered).to have_tag('form', :with => { :action => '/accounts/99', :method => 'post' })
  end

  it "should ask for an account name" do
    render
    expect(rendered).to have_tag('input', :with => { :name => 'account[name]' })
  end

  it "should ask for a keyId" do
    render
    expect(rendered).to have_tag('input', :with => { :name => 'account[keyId]' })
  end

  it "should ask for a vCode" do
    render
    expect(rendered).to have_tag('input', :with => { :name => 'account[vCode]' })
  end
end
