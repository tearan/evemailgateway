require 'spec_helper'

describe "characters/show" do
  before(:each) do
    @character = assign(:character, stub_model(Character,
      :name => "Character Name"
    ))
    @character.stub(:eve_mails).and_return([stub_model(EveMail, :subject => "Email subject")])
  end

  it "shows the character name" do
    render
    rendered.should match(/Character Name/)
  end

  it "shows a list of emails" do
    render
    rendered.should have_selector("td", :text => "Email subject") 
  end
end
