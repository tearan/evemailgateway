require 'spec_helper'

describe "users/show" do
  before(:each) do
    @user = assign(:user, stub_model(User,
      :first_name => "First",
      :last_name => "Last",
      :email => "first_last@example.com"
    ))
  end

  it "shows the user's name" do
    render
    rendered.should match(/#{@user.first_name} #{@user.last_name}/)
  end

  it "tells the user where their in game mail will be forwarded to" do
    render
    rendered.should match(/#{@user.email}/)
  end

  it "has a link ot add an Eve account" do
    render
    rendered.should have_link("Add an account", :href => new_account_path)
  end

  context "with characters" do
    before(:each) do
      @user.stub(:characters).and_return([stub_model(Character, :id => 99, :name => "Character name")])
    end

    it "shows a list of the user's characters" do
      render
      rendered.should match(/Character name/)
    end

    it "shows links to user's character's page" do
      render
      rendered.should have_link( "Character name", :href => character_path("99") )
    end
  end
end
