Clearance.configure do |config|
  config.mailer_sender = 'evemailgateway@example.com'
  config.secure_cookie = false
end
