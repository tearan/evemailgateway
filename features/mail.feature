Feature: read evemail and send email to owner of the character
	 In order to keep up with Eve when I am away from the game
         I want this application to email me every time I get a new ingame email

  Scenario: read evemail and send email   
    When my character receives a new ingame email 
    Then an email is send to my email address with the contents of the evemail
