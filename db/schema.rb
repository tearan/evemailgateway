# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131005060618) do

  create_table "accounts", force: true do |t|
    t.string   "name",       null: false
    t.string   "keyId",      null: false
    t.string   "vCode",      null: false
    t.integer  "user_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "accounts", ["user_id"], name: "index_accounts_on_user_id"

  create_table "characters", id: false, force: true do |t|
    t.string   "characterID", null: false
    t.string   "name",        null: false
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "characters", ["account_id"], name: "index_characters_on_account_id"
  add_index "characters", ["characterID"], name: "index_characters_characterID", unique: true

  create_table "eve_mails", id: false, force: true do |t|
    t.integer  "messageID",          null: false
    t.string   "characterID",        null: false
    t.integer  "senderID"
    t.datetime "sentDate"
    t.string   "title"
    t.string   "toCorpOrAllianceID"
    t.string   "toCharacterIDs"
    t.string   "toListID"
    t.text     "body"
    t.datetime "forwarded_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "eve_mails", ["characterID"], name: "index_eve_mails_characterID"
  add_index "eve_mails", ["messageID"], name: "index_eve_mails_messageID", unique: true

  create_table "users", force: true do |t|
    t.string   "first_name",                     null: false
    t.string   "last_name",                      null: false
    t.string   "email",                          null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "encrypted_password", limit: 128
    t.string   "confirmation_token", limit: 128
    t.string   "remember_token",     limit: 128
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["remember_token"], name: "index_users_on_remember_token"

end
