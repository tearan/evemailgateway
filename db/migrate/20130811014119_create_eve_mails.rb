class CreateEveMails < ActiveRecord::Migration
  def change
    create_table :eve_mails, id: false do |t|
      t.integer  :messageID, null:false
      t.string   :characterID, null:false
      t.integer  :senderID
      t.datetime :sentDate
      t.string   :title
      t.string   :toCorpOrAllianceID
      t.string   :toCharacterIDs
      t.string   :toListID
      t.text     :body
      t.datetime :forwarded_at

      t.timestamps
    end

    add_index "eve_mails", ["messageID"], unique: true, name: "index_eve_mails_messageID"
    add_index "eve_mails", ["characterID"], name: "index_eve_mails_characterID"
  end
end
