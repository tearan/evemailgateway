class CreateCharacters < ActiveRecord::Migration
  def change
    create_table :characters, id: false do |t|
      t.string :characterID, null: false
      t.string :name, null: false
      t.references :account, index: true

      t.timestamps
    end

    add_index "characters", ["characterID"], unique: true, name: "index_characters_characterID"
  end
end
