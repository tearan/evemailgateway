class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :name, null: false
      t.string :keyId, null: false
      t.string :vCode, null: false
      t.references :user, null: false, index: true

      t.timestamps
    end
  end
end
